const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');

const app = express();

// routes
const {authRouter} = require('./src/controllers/authController');
const {usersRouter} = require('./src/controllers/usersController');
const {loadsRouter} = require('./src/controllers/loadsController');
const {trucksRouter} = require('./src/controllers/trucksControllers');
// custom error
const {CustomError} = require('./src/models/errorModel');

app.use(express.json());
app.use(morgan('tiny'));
require('dotenv').config();

app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);
app.use('/api/loads', loadsRouter);
app.use('/api/trucks', trucksRouter);

app.use((req, res, next) => {
  res.status(404).json({message: 'Not found'});
});

app.use((err, req, res, next) => {
  if (err instanceof CustomError) {
    return res.status(err.status).json({
      message: err.message,
    });
  }
  res.status(500).json({
    message: err.message,
  });
});

const launch = async () => {
  try {
    await mongoose.connect(`mongodb+srv://anonymus-admin:${process.env.DBPASSWORD}@notepadx.v2sdn.mongodb.net/test`, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });
    app.listen(process.env.PORT);
  } catch (error) {
    console.error(`Error on server startup: ${error.message}`);
  }
};

launch();
