const Joi = require('joi');
const jwt = require('jsonwebtoken');

const {
  AuthorizationError,
  InvalidRequestError,
  throwInvalidReqError,
  ForbiddenError,
} = require('../models/errorModel');

const registrationValidator = async (req, res, next) => {
  const validationSchema = Joi.object({
    email: Joi.string().email().required(),
    role: Joi.string().valid('DRIVER', 'SHIPPER').required(),
    password: Joi.string().required(),
  });

  try {
    await validationSchema.validateAsync(req.body);
    next();
  } catch (error) {
    next(error);
  }
};

const loadValidator = async (req, res, next) => {
  const validationSchema = Joi.object({
    name: Joi.string().required(),
    payload: Joi.number().required(),
    pickup_address: Joi.string().required(),
    delivery_address: Joi.string().required(),
    dimensions: Joi.object({
      length: Joi.number().required(),
      width: Joi.number().required(),
      height: Joi.number().required(),
    }),
  });

  try {
    await validationSchema.validateAsync(req.body);
    next();
  } catch (error) {
    next(new InvalidRequestError('Incorrect shipment data'));
  }
};

const loginValidator = async (req, res, next) => {
  const validationSchema = Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().required(),
  });

  try {
    await validationSchema.validateAsync(req.body);
    next();
  } catch (error) {
    next(new AuthorizationError());
  }
};

const emailValidator = async (req, res, next) => {
  const validationSchema = Joi.object({
    email: Joi.string().email().required(),
  });

  try {
    await validationSchema.validateAsync(req.body);
    next();
  } catch (error) {
    next(new InvalidRequestError('Please, provide a proper email'));
  }
};

const authValidator = async (req, res, next) => {
  try {
    const {authorization} = req.headers;
    if (!authorization) {
      const msg = 'Please, provide "authorization" header';
      throw new AuthorizationError(msg);
    }
    const [, token] = authorization.split(' ');
    if (!token) {
      throw new AuthorizationError('Please, include token to request');
    }
    const tokenPayload = jwt.verify(token, 'superSuperBigSecret1');
    if (!tokenPayload) {
      throw new AuthorizationError('Invalid token');
    }
    req.user = tokenPayload;
    next();
  } catch (err) {
    next(err);
  }
};

const truckTypeValidator = async (req, res, next) => {
  const truckTypes = ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'];
  const validationSchema = Joi.object({
    type: Joi.string().valid(...truckTypes).required(),
  });
  try {
    await validationSchema.validateAsync(req.body);
    next();
  } catch (error) {
    next(error);
  }
};

const driverRoleValidator = async (req, res, next) => {
  try {
    const authorizedRole = 'DRIVER';
    if (req.user.role !== authorizedRole) {
      throw new ForbiddenError('Action is not allowed for this type of user');
    }
    next();
  } catch (err) {
    next(err);
  }
};

const shipperRoleValidator = async (req, res, next) => {
  try {
    const authorizedRole = 'SHIPPER';
    if (req.user.role !== authorizedRole) {
      throw new ForbiddenError('Action is not allowed for this type of user');
    }
    next();
  } catch (err) {
    next(err);
  }
};

const roleValidator = async (req, res, next) => {
  try {
    if (req.user.role !== 'DRIVER' && req.user.role !== 'SHIPPER') {
      throwInvalidReqError('Action is not allowed for this type of user');
    }
    const stats = req.query.status;
    if (req.user.role === 'DRIVER' && (stats === 'NEW' || stats === 'POSTED')) {
      throw new ForbiddenError('Driver is not allowed to perform this action');
    }
    next();
  } catch (err) {
    next(err);
  }
};

// const statusValidator = async (req, res, next) => {
//   try {
//     if (!req.query.status) {
//       throw new InvalidRequestError('Status parameter is required');
//     }
//     next();
//   } catch (error) {
//     next(error);
//   }
// };

module.exports = {
  registrationValidator,
  loginValidator,
  emailValidator,
  authValidator,
  truckTypeValidator,
  driverRoleValidator,
  shipperRoleValidator,
  loadValidator,
  roleValidator,
  // statusValidator,
};
