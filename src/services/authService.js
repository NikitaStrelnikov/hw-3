const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const {User} = require('../models/userModel');

require('dotenv').config();

const {
  throwInvalidReqError,
} = require('../models/errorModel');

const registrationService = async ({email, role, password}) => {
  const user = new User({
    email,
    role,
    password: await bcrypt.hash(password, 10),
    created_date: new Date().toISOString(),
  });
  await user.save();
};

const loginService = async ({email, password}) => {
  const user = await User.findOne({email});

  if (!user) {
    throwInvalidReqError();
  }

  if (!(await bcrypt.compare(password, user.password))) {
    throwInvalidReqError();
  }

  const token = jwt.sign({
    _id: user._id,
    email: user.email,
    role: user.role,
    password: user.password,
  },
  'superSuperBigSecret1',
  );

  return token;
};

module.exports = {
  registrationService,
  loginService,
};
