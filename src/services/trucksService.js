const {Truck, TruckParams} = require('../models/truckModel');
const {
  InvalidRequestError,
} = require('../models/errorModel');

const isTruckSuitable = (truckParameters, vectorFormations) => {
  let isSuitable;
  for (let i = 0; i < vectorFormations.length; i++) {
    isSuitable = true;
    for (let j = 0; j < vectorFormations[0].length; j++) {
      if (vectorFormations[i][j] > truckParameters[j]) {
        isSuitable = false;
        break;
      }
    }
    if (isSuitable) {
      return true;
    }
  }
  return false;
};

const dimensionsAreSuitable = (dims, truckType) => {
  const vectorFormations = [
    [dims.width, dims.height, dims.length],
    [dims.width, dims.length, dims.height],
    [dims.height, dims.width, dims.length],
    [dims.height, dims.length, dims.width],
    [dims.length, dims.width, dims.height],
    [dims.length, dims.height, dims.width],
  ];

  switch (truckType) {
    case 'SPRINTER':
      return isTruckSuitable(
          Object.values(TruckParams.getSprinterDims()),
          vectorFormations,
      );
    case 'SMALL STRAIGHT':
      return isTruckSuitable(
          Object.values(TruckParams.getSmallStraightDims()),
          vectorFormations,
      );
    case 'LARGE STRAIGHT':
      return isTruckSuitable(
          Object.values(TruckParams.getLargeStraightDims()),
          vectorFormations,
      );
    default:
      break;
  }
};

// determining necessary truck type by load dimensions
const defineTruckTypeByLoad = (loadWeight, loadDimensions) => {
  const sprinter = 'SPRINTER';
  const smallStraight = 'SMALL STRAIGHT';
  const largeStraight = 'LARGE STRAIGHT';

  if (loadWeight <= TruckParams.getSprinterLoadCapacity()) {
    if (dimensionsAreSuitable(loadDimensions, sprinter)) {
      return sprinter;
    }
  }
  if (loadWeight <= TruckParams.getSmallStraightLoadCapacity()) {
    if (dimensionsAreSuitable(loadDimensions, smallStraight)) {
      return smallStraight;
    }
  }
  if (loadWeight <= TruckParams.getLargeStraightLoadCapacity()) {
    if (dimensionsAreSuitable(loadDimensions, largeStraight)) {
      return largeStraight;
    }
  }
  return;
};

const addTruckForUser = async (type, user) => {
  const truck = new Truck({
    created_by: user._id,
    type,
    status: 'IS',
    created_date: new Date().toISOString(),
  });
  await truck.save();
};

const getDriversTrucks = async (userId) => {
  const trucks = await Truck.find({created_by: userId});
  return trucks;
};

const assignTruckToUserById = async (truckId, userId) => {
  const usersTrucks = await Truck.find({created_by: userId});
  usersTrucks.forEach(async (truck) => {
    if (truck.status === 'OL') {
      const msg = 'You can not assign new truck to yourself during a mission';
      throw new InvalidRequestError(msg);
    }
  });
  await Truck.findOneAndUpdate({
    created_by: userId,
    assigned_to: userId,
  }, {assigned_to: null}, {
    useFindAndModify: false,
  });
  await Truck.findOneAndUpdate({_id: truckId}, {assigned_to: userId}, {
    useFindAndModify: false,
  });
};

const getTruckForUserById = async (truckId, userId) => {
  const truck = await Truck.findOne({_id: truckId, created_by: userId});
  return truck;
};

const checkIfTruckIsNotAssigned = async (truckId) => {
  const truck = await Truck.findOne({_id: truckId, assigned_to: null});
  if (!truck) {
    const msg = 'You can not modify info about an assigned truck';
    throw new InvalidRequestError(msg);
  }
};

const updateTruckForDriverById = async (truckId, truckDetails) => {
  await checkIfTruckIsNotAssigned(truckId);
  await Truck.findOneAndUpdate({
    _id: truckId,
    assigned_to: null,
  }, {...truckDetails}, {
    useFindAndModify: false,
  });
};

const deleteTruckForDriverById = async (truckId) => {
  await checkIfTruckIsNotAssigned(truckId);
  await Truck.findOneAndDelete({
    _id: truckId,
    assigned_to: null,
  }, {
    useFindAndModify: false,
  });
};

const findAvailableTruckForLoad = async (loadWeight, loadDimensions) => {
  const truckType = defineTruckTypeByLoad(loadWeight, loadDimensions);
  const availableTruck = await Truck.findOne({
    type: truckType,
  });
  return availableTruck;
};

const changeTruckStatusById = async (truckId, status) => {
  await Truck.findOneAndUpdate({_id: truckId}, {status}, {
    useFindAndModify: false,
  });
};

const changeAssignedTruckStatusByUserId = async (userId, status) => {
  await Truck.findOneAndUpdate({
    assigned_to: userId,
  }, {status}, {
    useFindAndModify: false,
  });
};

const findTruckAssignedToDriver = async (assignedTo) => {
  const truck = await Truck.findOne({assigned_to: assignedTo});
  return truck;
};

module.exports = {
  addTruckForUser,
  getDriversTrucks,
  assignTruckToUserById,
  getTruckForUserById,
  updateTruckForDriverById,
  deleteTruckForDriverById,
  findAvailableTruckForLoad,
  changeTruckStatusById,
  findTruckAssignedToDriver,
  changeAssignedTruckStatusByUserId,
};
