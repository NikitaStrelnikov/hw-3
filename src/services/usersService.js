const {User} = require('../models/userModel');
const bcrypt = require('bcrypt');
require('dotenv').config();

const getProfileInfo = async (id) => {
  const user = await User.findById(id);
  if (user) {
    user.password = undefined;
    user.__v = undefined;
  }
  return user;
};

const updateUsersPswrd = async ({email, newPassword}) => {
  const filter = {email};
  const update = {
    password: await bcrypt.hash(newPassword, 10),
  };
  await User.findOneAndUpdate(filter, update, {
    useFindAndModify: false,
  });
};

const deleteUser = async (email) => {
  await User.findOneAndDelete({email}, {
    useFindAndModify: false,
  });
};

module.exports = {
  getProfileInfo,
  updateUsersPswrd,
  deleteUser,
};
