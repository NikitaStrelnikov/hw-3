const {Load} = require('../models/loadModel');


const addNewLoadForUser = async (userId, load) => {
  const newLoad = new Load({
    ...load,
    created_by: userId,
    assigned_to: null,
    status: 'NEW',
    state: null,
    logs: [],
    created_date: new Date().toISOString(),
  });
  await newLoad.save();
};
// status, limit, offset, req.user._id
const getLoadsForDriver = async (status, limit, offset, userId) => {
  status = status || ['ASSIGNED', 'SHIPPED'];
  const loads = await Load
      .find({
        assigned_to: userId,
        status,
      })
      .skip(offset)
      .limit(limit);
  return loads;
};

const getLoadsForShipper = async (status, limit, offset, userId) => {
  if (status) {
    const loads = await Load
        .find({
          created_by: userId,
          status,
        })
        .skip(offset)
        .limit(limit);
    return loads;
  } else {
    const loads = await Load
        .find({
          created_by: userId,
        })
        .skip(offset)
        .limit(limit);
    return loads;
  }
};

const getDriversActiveLoad = async (userId) => {
  const activeLoad = await Load.findOne({
    assigned_to: userId,
    status: 'ASSIGNED',
  });
  return activeLoad;
};

const getUsersLoadById = async (loadId) => {
  const load = await Load.findOne({_id: loadId});
  return load;
};

const updateUsersLoadById = async (loadId, userId, updateInfo) => {
  const load = await Load.findOneAndUpdate({
    _id: loadId,
    status: 'NEW',
    created_by: userId,
  }, {
    ...updateInfo,
  }, {
    useFindAndModify: false,
  });
  return load;
};

const deleteUsersLoadById = async (loadId, userId) => {
  const load = await Load.findOneAndDelete({
    _id: loadId,
    status: 'NEW',
    created_by: userId,
  }, {
    useFindAndModify: false,
  });
  return load;
};

const changeLoadStatusById = async (loadId, status, userId = null) => {
  await Load.findByIdAndUpdate(loadId, {
    status,
    assigned_to: userId,
  }, {
    useFindAndModify: false,
  });
};

const iterateToNextLoadState = async (userId) => {
  const load = await Load.findOne({assigned_to: userId});
  const state = load.state;
  let nextState;
  switch (state) {
    case null:
      nextState = 'En route to Pick Up';
      break;
    case 'En route to Pick Up':
      nextState = 'Arrived to Pick Up';
      break;
    case 'Arrived to Pick Up':
      nextState = 'En route to delivery';
      break;
    case 'En route to delivery':
      nextState = 'Arrived to delivery';
      await Load.findOneAndUpdate({
        assigned_to: userId,
      },
      {status: 'SHIPPED'},
      {useFindAndModify: false},
      );
      break;
    default:
      break;
  }
  // const nextState = loadStates.filter((someState, i) => {
  //   if (state === someState) {
  //     return loadStates[i+1];
  //   } else if (!state) {
  //     return loadStates[1];
  //   }
  // });
  await Load.findOneAndUpdate({
    assigned_to: userId,
  },
  {state: nextState},
  {useFindAndModify: false},
  );
  return nextState;
};

const writeMsgToLoadLogs = async (loadId, userId = null) => {
  const load = await Load.findOne({_id: loadId});
  const {logs} = load;
  switch (logs.length) {
    case 0:
      logs.push({
        message: `Load assigned to driver with id ${userId}`,
        time: new Date().toISOString(),
      });
      break;
    case 1:
      logs.push({
        message: `Driver with id ${userId} arrived to pick up the load`,
        time: new Date().toISOString(),
      });
      break;
    case 2:
      logs.push({
        message: `Driver with id ${userId} is delivering the load`,
        time: new Date().toISOString(),
      });
      break;
    case 3:
      logs.push({
        message: `Driver with id ${userId} delivered the load`,
        time: new Date().toISOString(),
      });
      break;
    default:
      break;
  }
  await Load.findOneAndUpdate({_id: loadId}, {logs}, {
    useFindAndModify: false,
  });
};

const findActiveLoadForShipperById = async (loadId) => {
  const load = await Load.findOne({_id: loadId, status: 'ASSIGNED'});
  return load;
};

module.exports = {
  addNewLoadForUser,
  getLoadsForDriver,
  getLoadsForShipper,
  getDriversActiveLoad,
  getUsersLoadById,
  updateUsersLoadById,
  deleteUsersLoadById,
  changeLoadStatusById,
  iterateToNextLoadState,
  writeMsgToLoadLogs,
  findActiveLoadForShipperById,
};
