/**
 * The root custom error-class
 */
class CustomError extends Error {
/**
 * @param {string} message The message text
*/
  constructor(message) {
    super(message);
    this.status = 500;
  }
}

/**
 * Invalid request error-class
 */
class InvalidRequestError extends CustomError {
/**
 * @param {string} message The message text
*/
  constructor(message = 'Invalid request') {
    super(message);
    this.status = 400;
  }
}

/**
 * Invalid credentials error-class
 */
class AuthorizationError extends CustomError {
/**
 * @param {string} message The message text
*/
  constructor(message = 'Invalid credentials') {
    super(message);
    this.status = 401;
  }
}
/**
 * Forbidden action error-class
 */
class ForbiddenError extends CustomError {
/**
 * @param {string} message The message text
*/
  constructor(message = 'Forbidden action') {
    super(message);
    this.status = 403;
  }
}

const throwInvalidReqError = (msg = 'Invalid email or password') => {
  throw new AuthorizationError(msg);
};

module.exports = {
  CustomError,
  InvalidRequestError,
  throwInvalidReqError,
  AuthorizationError,
  ForbiddenError,
};
