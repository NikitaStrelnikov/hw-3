const mongoose = require('mongoose');

const Truck = mongoose.model('Truck', {
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: null,
  },
  type: {
    type: String,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
    required: true,
  },
  status: {
    type: String,
    enum: ['OL', 'IS'],
    required: true,
  },
  created_date: {
    type: Date,
    required: true,
    default: Date.now(),
  },
});

/**
 * The root custom error-class
 */
class TruckParams {
/**
* @param {void} method excepts no parameters
* @return {object} object containing sprinter-class car parameters
*/
  static getSprinterDims() {
    return {
      length: 300,
      width: 250,
      height: 170,
    };
  }
  /**
* @param {void} method excepts no parameters
* @return {number} idenntifying sprinter car load capacity
*/
  static getSprinterLoadCapacity() {
    return 1700;
  }
  /**
* @param {void} method excepts no parameters
* @return {object} object containing small-straigh-class car parameters
*/
  static getSmallStraightDims() {
    return {
      length: 500,
      width: 250,
      height: 170,
    };
  }
  /**
* @param {void} method excepts no parameters
* @return {number} idenntifying small straight car load capacity
*/
  static getSmallStraightLoadCapacity() {
    return 2500;
  }
  /**
* @param {void} method excepts no parameters
* @return {object} object containing large-straigh-class car parameters
*/
  static getLargeStraightDims() {
    return {
      length: 700,
      width: 350,
      height: 200,
    };
  }
  /**
* @param {void} method excepts no parameters
* @return {number} idenntifying large straight car load capacity
*/
  static getLargeStraightLoadCapacity() {
    return 4000;
  }
}

module.exports = {
  Truck,
  TruckParams,
};
