const mongoose = require('mongoose');

const loadStates = [
  null,
  'En route to Pick Up',
  'Arrived to Pick Up',
  'En route to delivery',
  'Arrived to delivery'];

const Load = mongoose.model('Load', {
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: null,
  },
  status: {
    type: String,
    required: true,
  },
  state: {
    type: String,
    default: null,
    // eslint-disable-next-line max-len
    enum: loadStates,
  },
  name: {
    type: String,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    type: mongoose.Schema.Types.Mixed,
  },
  logs: [mongoose.Schema.Types.Mixed],
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = {
  Load,
  loadStates,
};
