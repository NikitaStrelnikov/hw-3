const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();

const {
  registrationService,
  loginService,
} = require('../services/authService');

const {asyncWrapper} = require('../utilities/apiUtilities');

const {
  registrationValidator,
  loginValidator,
  emailValidator,
} = require('../middlewares/validationMiddleware');

router.post('/register', registrationValidator, asyncWrapper(async (req, res)=>{
  const {
    email,
    role,
    password,
    createdDate,
  } = req.body;

  await registrationService({email,
    role,
    password,
    createdDate,
  });
  res.json({
    message: 'Profile created successfully',
  });
}));

router.post('/login', loginValidator, asyncWrapper(async (req, res) => {
  const {
    email,
    password,
  } = req.body;

  const token = await loginService({email, password});

  res.json({jwt_token: token});
}));

router.post('/forgot_password',
    emailValidator,
    asyncWrapper(async (req, res) => {
      // const {email} = req.body;
      res.json({
        message: 'New password sent to your email address',
      });
    }));

module.exports = {
  authRouter: router,
};
