const express = require('express');
// const jwt = require('jsonwebtoken');
const {
  authValidator,
  truckTypeValidator,
  driverRoleValidator,
} = require('../middlewares/validationMiddleware');
const {asyncWrapper} = require('../utilities/apiUtilities');
// eslint-disable-next-line new-cap
const router = express.Router();

const {
  addTruckForUser,
  getDriversTrucks,
  assignTruckToUserById,
  getTruckForUserById,
  updateTruckForDriverById,
  deleteTruckForDriverById,
} = require('../services/trucksService');
// creating new truck for user
router.post('/',
    authValidator,
    truckTypeValidator,
    driverRoleValidator,
    asyncWrapper(async (req, res) => {
      const {type} = req.body;
      await addTruckForUser(type, req.user);
      res.json({
        message: 'Truck created successfully',
      });
    }));
// sending all the trucks that user has created
router.get('/',
    authValidator,
    driverRoleValidator,
    asyncWrapper(async (req, res) => {
      const trucks = await getDriversTrucks(req.user._id);
      res.json({trucks});
    }),
);
// assigning a truck to user by the trucks id
router.post('/:id/assign',
    authValidator,
    driverRoleValidator,
    asyncWrapper(async (req, res) => {
      await assignTruckToUserById(req.params.id, req.user._id);
      res.json({
        message: 'Truck assigned successfully',
      });
    }),
);
// getting a truck by id
router.get('/:id',
    authValidator,
    driverRoleValidator,
    asyncWrapper(async (req, res) => {
      const truck = await getTruckForUserById(req.params.id, req.user._id);
      res.json({
        truck,
      });
    }));
// updating info about a truck that is not assigned to
// to the user that made this request
router.put('/:id',
    authValidator,
    driverRoleValidator,
    asyncWrapper(async (req, res) => {
      await updateTruckForDriverById(req.params.id, req.body);
      res.json({
        message: 'Truck details changed successfully',
      });
    }));
// deleting a truck that is not assigned to
// to the user that made this request
router.delete('/:id',
    authValidator,
    driverRoleValidator,
    asyncWrapper(async (req, res) => {
      await deleteTruckForDriverById(req.params.id);
      res.json({
        message: 'Truck deleted successfully',
      });
    }));
module.exports = {
  trucksRouter: router,
};
