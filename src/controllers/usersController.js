const express = require('express');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
require('dotenv').config();

const {
  getProfileInfo,
  updateUsersPswrd,
  deleteUser,
} = require('../services/usersService');
const {asyncWrapper} = require('../utilities/apiUtilities');
const {
  AuthorizationError,
  throwInvalidReqError,
} = require('../models/errorModel.js');
// eslint-disable-next-line new-cap
const router = express.Router();

router.get('/me', asyncWrapper(async (req, res) => {
  const {
    authorization,
  } = req.headers;

  if (!authorization) {
    throw new AuthorizationError('Please, provide "authorization" header');
  }

  const [, token] = authorization.split(' ');

  if (!token) {
    throw new AuthorizationError('Please, include token to request');
  }

  const tokenPayload = jwt.verify(token, 'superSuperBigSecret1');
  const user = await getProfileInfo(tokenPayload._id);
  if (!user) {
    throw new AuthorizationError(`Couldn't find this user`);
  }
  res.json({
    user,
  });
}));

router.patch('/me/password', asyncWrapper(async (req, res) => {
  const {
    oldPassword,
    newPassword,
  } = req.body;

  const {authorization} = req.headers;

  if (!authorization) {
    throw new AuthorizationError('Please, provide "authorization" header');
  }

  if (!newPassword || !oldPassword) {
    const msg = 'Please, add new and old password to request';
    throw new AuthorizationError(msg);
  }

  const [, token] = authorization.split(' ');

  if (!token) {
    throw new AuthorizationError('Please, include token to request');
  }

  const tokenPayload = jwt.verify(token, 'superSuperBigSecret1');
  if (!(await bcrypt.compare(oldPassword, tokenPayload.password))) {
    throwInvalidReqError('Wrong old password');
  }

  await updateUsersPswrd({newPassword, email: tokenPayload.email});
  res.json({
    message: 'Password changed successfully',
  });
}));

router.delete('/me', asyncWrapper(async (req, res) => {
  const {authorization} = req.headers;

  if (!authorization) {
    throw new AuthorizationError('Please, provide "authorization" header');
  }

  const [, token] = authorization.split(' ');

  if (!token) {
    throw new AuthorizationError('Please, include token to request');
  }

  const tokenPayload = jwt.verify(token, 'superSuperBigSecret1');
  await deleteUser(tokenPayload.email);
  res.json({
    message: 'Profile deleted successfully',
  });
}));

module.exports = {
  usersRouter: router,
};
