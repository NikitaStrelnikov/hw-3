const express = require('express');
const {asyncWrapper} = require('../utilities/apiUtilities');
const {
  authValidator,
  shipperRoleValidator,
  loadValidator,
  roleValidator,
  driverRoleValidator,
} = require('../middlewares/validationMiddleware');
const {
  addNewLoadForUser,
  getLoadsForDriver,
  getLoadsForShipper,
  getDriversActiveLoad,
  getUsersLoadById,
  updateUsersLoadById,
  deleteUsersLoadById,
  changeLoadStatusById,
  iterateToNextLoadState,
  writeMsgToLoadLogs,
  findActiveLoadForShipperById,
} = require('../services/loadsService');
const {
  findAvailableTruckForLoad,
  changeTruckStatusById,
  findTruckAssignedToDriver,
  changeAssignedTruckStatusByUserId,
} = require('../services/trucksService');
// eslint-disable-next-line new-cap
const router = express.Router();

// creating a new load
router.post('/',
    authValidator,
    shipperRoleValidator,
    loadValidator,
    asyncWrapper(async (req, res) => {
      await addNewLoadForUser(req.user._id, req.body);
      res.send({
        message: 'Load created successfully',
      });
    }));
// getting all of users loads (different for drivers and shippers)
router.get('/',
    authValidator,
    roleValidator,
    asyncWrapper(async (req, res) => {
      let loads;
      let {
        status,
        limit,
        offset,
      } = req.query;
      // checking if "limit" parameter was set and has acceptable value
      limit = limit ? limit : 10;
      limit = limit <= 50 ? limit : 50;
      // checking if "offset" parameter was set
      offset = offset ? offset : 0;
      if (req.user.role === 'DRIVER') {
        loads = await getLoadsForDriver(status, +limit, +offset, req.user._id);
      } else if (req.user.role === 'SHIPPER') {
        loads = await getLoadsForShipper(status, +limit, +offset, req.user._id);
      }
      res.json({
        loads,
      });
    }));
// getting drivers active load
router.get('/active',
    authValidator,
    driverRoleValidator,
    asyncWrapper(async (req, res) => {
      const activeLoad = await getDriversActiveLoad(req.user._id);
      if (!activeLoad) {
        res.json({
          load: {},
        });
      } else {
        res.json({
          load: activeLoad,
        });
      }
    }));
// getting shippers load by id
router.get('/:id',
    authValidator,
    asyncWrapper(async (req, res) => {
      const load = await getUsersLoadById(req.params.id);
      if (!load) {
        res.status(404).json({
          message: 'No load with this id found',
        });
      } else {
        res.json({
          load,
        });
      }
    }));
// updating users load by id
router.put('/:id',
    authValidator,
    shipperRoleValidator,
    asyncWrapper(async (req, res) => {
      const updatedLoad = await updateUsersLoadById(
          req.params.id,
          req.user._id,
          req.body,
      );
      if (!updatedLoad) {
        res.status(404).json({
          message: `Could not find shipment with passed id and 'NEW' status`,
        });
      } else {
        res.json({
          message: 'Load details changed successfully',
        });
      }
    }));
// deleting users load by id
router.delete('/:id',
    authValidator,
    shipperRoleValidator,
    asyncWrapper(async (req, res) => {
      const deletedLoad = await deleteUsersLoadById(
          req.params.id,
          req.user._id,
      );
      if (!deletedLoad) {
        res.status(404).json({
          message: `Could not find shipment with passed id and 'NEW' status`,
        });
      } else {
        res.json({
          message: 'Load deleted successfully',
        });
      }
    }));
// Post a user's load by id, search for drivers
// (available only for shipper role)
router.post('/:id/post',
    authValidator,
    shipperRoleValidator,
    asyncWrapper(async (req, res) => {
      await changeLoadStatusById(req.params.id, 'POSTED');
      const load = await getUsersLoadById(req.params.id);
      const truck = await findAvailableTruckForLoad(
          load.payload,
          load.dimensions,
      );
      const userId = truck.assigned_to;
      // rolling back to 'NEW' status if there's no truck available
      if (!truck) {
        await changeLoadStatusById(req.params.id, 'NEW');
      } else {
        await changeLoadStatusById(
            req.params.id,
            'ASSIGNED',
            userId);
        await writeMsgToLoadLogs(load._id, userId);
        await changeTruckStatusById(truck._id, 'OL');
        await iterateToNextLoadState(userId);
      }
      // dimensions payload
      if (!truck) {
        res.status(404).json({
          message: 'No truck available',
        });
      } else {
        res.json({
          message: 'Load posted successfully',
          driver_found: true,
        });
      }
    }));

router.patch('/active/state',
    authValidator,
    driverRoleValidator,
    asyncWrapper(async (req, res) => {
      const newLoadState = await iterateToNextLoadState(req.user._id);
      if (newLoadState === 'Arrived to delivery') {
        await changeAssignedTruckStatusByUserId(req.user._id, 'IS');
      }
      if (newLoadState) {
        res.json({
          message: `Load state changed to '${newLoadState}'`,
        });
      } else {
        res.json({
          message: `Cannot go to next state. 
          Maybe you are have finished your job?`,
        });
      }
    }));

router.get('/:id/shipping_info',
    authValidator,
    shipperRoleValidator,
    asyncWrapper(async (req, res) => {
      const activeLoad = await findActiveLoadForShipperById(req.params.id);
      const activeTruck = await findTruckAssignedToDriver(
          activeLoad.assigned_to,
      );
      if (!activeLoad) {
        res.status(400).json({
          message: 'No active loads',
        });
      } else {
        res.json({
          load: activeLoad,
          truck: activeTruck,
        });
      }
    }));

module.exports = {
  loadsRouter: router,
};
